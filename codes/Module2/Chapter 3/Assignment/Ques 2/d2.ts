function check()
{
    var x: HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var y: HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
    var z: HTMLInputElement = <HTMLInputElement>document.getElementById("t3");

    var a: number = +x.value;
    var b: number = +y.value;
    var c: number = +z.value;

    if(a==b && b==c)
    {
        document.getElementById("p1").innerHTML = "<b> The triangle is Equilateral...!!!</b>";
    }
    else if( (a==b && a!=c) || (b==c && b!=a) || (a==c && c!=b ) )
    {
        document.getElementById("p1").innerHTML = "The triangle is Isoceles...!!!";
        if(Math.pow(a,2) ==  (Math.pow(b,2) + Math.pow(c,2))  || Math.pow(b,2) ==  (Math.pow(a,2) + Math.pow(c,2)) 
        || Math.pow(c,2) ==  (Math.pow(b,2) + Math.pow(a,2)) )
        {
            document.getElementById("p2").innerHTML = "The triangle is Right Angle ...!!!";
        }
    }
    else if(a!=b && b!=c && a!=c)
    {
        document.getElementById("p1").innerHTML = "The triangle is Scalene...!!!";

        if(Math.pow(a,2) ==  (Math.pow(b,2) + Math.pow(c,2))  || Math.pow(b,2) ==  (Math.pow(a,2) + Math.pow(c,2)) 
        || Math.pow(c,2) ==  (Math.pow(b,2) + Math.pow(a,2)) )
        {
            document.getElementById("p2").innerHTML = "The triangle is Right Angle ...!!!";
        }
    }
}